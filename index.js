const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const app = express()
const flash = require('connect-flash')
const session = require('express-session')
var MongoStore = require('connect-mongo')(session)
let compression = require('compression')
let morgan = require('morgan')
let expressValidator = require('express-validator');
const mongoose = require('mongoose')
let passport = require('passport')
const PORT = process.env.PORT || 8080

mongoose.connect('mongodb://localhost:27017/pointSystem')
let db = mongoose.connection

db.once('open', () =>{
  console.log('Connected to db');
})

db.on('error', (err) => {
  console.log(err);
})

app.use(morgan('dev'))
app.use(compression())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(express.static(path.join(__dirname, 'public')))
app.use(session({
  secret: '',
  resave: true,
  saveUninitialized: true,
  maxAge: new Date(Date.now() + 2600000),
  store: new MongoStore({
    mongooseConnection:db
   })
}))

app.use(require('connect-flash')());
app.use((req, res, next) => {
  res.locals.messages = require('express-messages')(req, res);
  next();
})

app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

require('./config/passport')(passport)
app.use(passport.initialize());
app.use(passport.session());
app.get('*', (req, res, next) => {
  res.locals.user = req.user || null
  next()
})



let tasks = require('./routes/tasks.js')
let users = require('./routes/users.js')

app.get('/', (req, res) => {
  res.redirect('/task')
  //res.sendFile('index.html')
})

app.use('/task', tasks)
app.use('/user', users)

app.listen(PORT, () => {
  console.log('Server running on port: '+PORT);
})
