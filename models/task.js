let mongoose = require('mongoose')

let taskSchema = mongoose.Schema({
  title: {type: String, required: true},
  content: {type: String, required: true},
  points: {type: Number, required: true},
  aCon: {type: Boolean, default: false},
  done: {type: Boolean, default: false},
  active: {type: Boolean, default: false},
  user_id: {type: String, default: ''},
  doneByUsername: {type: String, default: ''}
})


let Task = module.exports = mongoose.model('Task', taskSchema)
