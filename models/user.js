let mongoose = require('mongoose')

let UserSchema = mongoose.Schema({
  username: {
    type: String,
    required: true
  },

  password: {
    type: String,
    required: true
  },

  points: {
    type: Number,
    default: 0
  },

  isAdmin: {
    type: Boolean,
    required: true,
    default: false
  },
  tasksCompleted: {
    type: Number,
    default: 0
  }
})

let User = module.exports = mongoose.model('User', UserSchema)
