$(document).ready(function () {
  $('.delete-task').on('click', (e) => {
    target = $(e.target)
    const id = target.attr('data-id')
    $.ajax({
      type: 'DELETE',
      url: '/task/'+id,
      success: (res) => {
        window.location.href = '/task'
      },
      error: (err) => {
        console.log(err);
      }
    })
  })
})
