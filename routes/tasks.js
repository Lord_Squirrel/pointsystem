const express = require('express')
const router = express.Router()

let Task = require('../models/task.js')
let User = require('../models/user')

router.get('/add',  ensureAdminAndAuth, function (req, res) {
  res.render('pages/add_task.pug')
})

router.get('/',  ensureAuth, function (req, res) {
  if (req.user.isAdmin) {
    res.redirect('/task/list')
    return;
  }

  Task.find({active: false, done: false}, (err, tasks) => {
     if (err) {
       console.log(err);
       return;
     }else {
       Task.find({user_id: req.user.id, active: true}, (error, userTasks) => {
         if (error) {
           console.log(error);
         }
         let user = {
           username: req.user.username,
           points: req.user.points,
           tasksCompleted: req.user.tasksCompleted
         }
         res.render('pages/billboard.pug', {
           tasks: tasks,
           userTasks: userTasks,
           user: user
         })
       })
     }
   })
})

router.get('/list',  ensureAdminAndAuth, function (req, res) {
  Task.find({}, (err, tasks) => {
     if (err) {
       console.log(err);
       return;
     }else {
       res.render('pages/tasks.pug', {
         tasks: tasks
       })
     }
   })
})

router.post('/list/:id', ensureAdminAndAuth, (req, res) => {
  Task.findById(req.params.id, (err, task) => {
    if (err) {
      console.log(err);
      return;
    }
    User.findById(task.user_id, (err, user) => {
      if (err) {
        console.log(err);
        return;
      }
      /*
      task.active = false
      task.aCon = false
      task.done = true
      task.doneByUsername = user.username
      user.points += task.points
      user.tasksCompleted += 1*/

      let newTask = {
        active: false,
        aCon: false,
        done: true,
        doneByUsername: user.username
      }

      let newUser = {
        points: user.points += task.points,
        tasksCompleted: user.tasksCompleted += 1
      }
      let q = {_id: req.params.id}


      Task.update(q, newTask, (err) => {
        if (err) {
          console.log(err);
        }

      })
      q = {_id: user.id}
      User.update(q, newUser, (err) => {
        if (err) {
          console.log(err);
        }
      })

      res.redirect('/task/list/')
    })
  })
})

router.post('/add', ensureAdminAndAuth, (req, res) => {
  req.checkBody('title', 'A title is required').notEmpty()
  req.checkBody('content', 'Content is required').notEmpty()
  req.checkBody('points', 'Points skal være imellem 1 og 50').optional({ checkFalsy: false}).isInt({min: 1, max: 50})


  let errors = req.validationErrors()

  if (errors) {
   res.render('pages/add_task', {
      errors: errors
    })
    //res.status(400).json(errors);
  }else {
    let task = new Task()
    task.title = req.body.title
    task.content = req.body.content
    task.points = req.body.points
    task.save((err) =>{
      if (err) {
        console.log(err);
        return;
      }else {
        res.redirect('/task')
        //res.status(200).json(task)
      }
    })
  }
})

router.delete('/:id', ensureAdminAndAuth, (req, res) => {

  let q = {_id: req.params.id}
  Task.findById(req.params.id, (err, task) => {
    if (err) {
      console.log(err);
      return;
    }
    Task.remove(q, (err) => {
      if (err) {
        console.log(err);
      }
      res.sendStatus(200)
    })
  })
})


router.get('/:id', ensureAuth, (req, res) => {
  Task.findById(req.params.id, (err, task) => {
    if (err) {
      console.log(err);
      return;
    }

    if (task.active) {
      if (task.user_id == req.user.id) {
        res.render('pages/task', {
          task: task
        })
      } else {
        res.redirect('/task/')
      }
    } else {
      res.render('pages/task', {
        task: task
      })
    }
  })
})

router.post('/:id', ensureAuth, (req, res) => {
  Task.findById(req.params.id, (err, task) => {
    if (err) {
      console.log(err);
      return;
    }
    if (!task.active && task.user_id == '') {
      let newTask = {
        active: true,
        user_id: req.user.id
      }
      /*
      task.active = true
      task.user_id = req.user.id*/
      let q = {_id: req.params.id};


      Task.update(q, newTask, (err) => {
        if (err) {
          console.log(err);
          return
        }
        res.redirect('/task/')

      })
    } else {
      req.flash('warning', 'Opgaven er allerede taget!')
      res.redirect('/task')
    }
  })
})

router.post('/:id/finish', ensureAuth, (req, res) => {
  Task.findById(req.params.id, (err, task) => {
    if (err) {
      console.log(err);
      return;
    }

    // Check if this is the user's task and it is active
    if (task.active && task.user_id == req.user.id && !task.aCon) {
      //task.aCon = true
      let newTask = {
        aCon: true
      }

      let q = {_id: req.params.id}
      Task.update(q, newTask, (err) => {
        if (err) {
          console.log(err);
          return;
        }
        res.redirect('/task/')
      })
    } else {
      res.sendStatus(403)
    }
  })
})

function ensureAuth(req, res, next) {
  if (req.isAuthenticated()) {
    return next()
  } else {
    req.flash('danger', 'Please login')
    res.redirect('/user/login')
  }
}

function ensureAdminAndAuth(req, res, next) {
  if (req.isAuthenticated() && req.user.isAdmin) {
    return next()
  } else {
    req.flash('danger', 'Access denied')
    res.redirect('/user/login')
  }
}

module.exports = router
