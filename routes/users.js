const express = require('express')
const router = express.Router()
let bcrypt = require('bcryptjs')
let passport = require('passport')
let User = require('../models/user')

router.get('/register', (req, res) => {
  res.render('pages/create_user')
})

router.post('/register', (req, res) => {
  let username = req.body.username
  let password = req.body.password
  let password2 = req.body.password2
  req.checkBody('username', 'Du mangler et brugernavn').notEmpty()
  req.checkBody('password', 'Du skal have et kodeord').notEmpty()
  req.checkBody('password2', 'Kodeordne passer ikke sammen').equals(req.body.password)
  let errors = req.validationErrors()

  if (errors) {
    res.render('pages/create_user', {
      errors: errors
    })
  } else {
    let newUser = new User({
      username: username,
      password: password
    })

    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) {
          console.log(err);
        }
        newUser.password = hash
        newUser.save((err) => {
          if (err) {
            console.log(err);
            return
          }
          req.flash('success', 'Success, din bruger er nu oprettet')
          res.redirect('/user/login')
        })
      })
    })
  }

})

router.get('/login', (req, res) => {
  res.render('pages/login.pug')
})

router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/task',
    failureRedirect: '/user/login',
    failureFlash: true
  })(req, res, next)
})

router.get('/logout', (req, res) => {
  req.logout()
  req.flash('success', 'Du er nu logget ud')
  res.redirect('/task')
})


module.exports = router
